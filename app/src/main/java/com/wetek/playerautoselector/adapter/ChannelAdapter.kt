package com.wetek.playerautoselector.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.wetek.playerautoselector.R
import com.wetek.playerautoselector.model.Channel
import com.wetek.playerautoselector.viewmodel.ChannelItemViewModel


class ChannelAdapter: RecyclerView.Adapter<ChannelAdapter.ViewHolder>() {
    lateinit var channelList:ArrayList<Channel>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: com.wetek.playerautoselector.databinding.ChannelItemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context),
            R.layout.channel_item, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(channelList[position])
    }

    override fun getItemCount(): Int {
        return if(::channelList.isInitialized) channelList.size else 0
    }

    fun updateChannelList(channelList:ArrayList<Channel>){
        this.channelList = channelList
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: com.wetek.playerautoselector.databinding.ChannelItemBinding):RecyclerView.ViewHolder(binding.root){
        private val viewModel = ChannelItemViewModel()

        fun bind(channel: Channel){
            viewModel.bind(channel)
            binding.viewModel = viewModel
        }
    }


}