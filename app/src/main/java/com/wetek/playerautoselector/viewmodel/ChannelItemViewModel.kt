package com.wetek.playerautoselector.viewmodel

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wetek.playerautoselector.model.Channel

class ChannelItemViewModel: ViewModel() {

    private val channelId = MutableLiveData<String>()
    private val channelSource = MutableLiveData<String>()
    private val textVisibility: MutableLiveData<Int> = MutableLiveData()
    private val imageVisibility:  MutableLiveData<Int> = MutableLiveData()


    fun bind(channel: Channel) {
        channelId.value = channel.id.toString()
        channelSource.value = channel.source
    }

    fun getChannelId(): MutableLiveData<String> {
        return channelId
    }
    fun getChannelSource(): MutableLiveData<String> {
        return channelSource
    }
    fun getTextPlayerVisibility(): MutableLiveData<Int>{
        when {
            !channelSource.value!!.contains("image") -> textVisibility.value = View.VISIBLE
            else -> textVisibility.value = View.GONE
        }
        return textVisibility
    }
    fun getImagePlayerVisibility(): MutableLiveData<Int>{
        when {
            channelSource.value!!.contains("image") -> imageVisibility.value = View.VISIBLE
            else -> imageVisibility.value = View.GONE
        }
        return imageVisibility
    }
}