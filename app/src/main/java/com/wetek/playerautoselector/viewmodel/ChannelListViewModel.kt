package com.wetek.playerautoselector.viewmodel

import androidx.lifecycle.ViewModel
import com.wetek.playerautoselector.adapter.ChannelAdapter
import com.wetek.playerautoselector.model.Channel


class ChannelListViewModel : ViewModel(){

    val channelListAdapter : ChannelAdapter =
        ChannelAdapter()
    val TEXT = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra, sem in tincidunt auctor, sem elit mollis libero, sed suscipit dolor est vitae nulla. Aliquam vel commodo nibh, a interdum magna."
    val IMAGE = "image"

    init {
        loadChannels()
    }

    private fun loadChannels(){
        val channelList : ArrayList<Channel> = ArrayList()
        for (i in 0..9){
            val rnds = (0..100).random()
            when  {
                rnds < 50 -> channelList.add(addTextChannel(i))
                else -> channelList.add(addImageChannel(i))
            }
        }
        channelListAdapter.updateChannelList(channelList)
    }

    private fun addTextChannel(id: Int): Channel {
        return Channel(id = id, source = TEXT)
    }

    private fun addImageChannel(id: Int): Channel {
        return Channel(id = id, source = IMAGE)
    }

    fun getChannelAdapter(): ChannelAdapter {
        return channelListAdapter
    }
}