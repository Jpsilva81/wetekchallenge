package com.wetek.playerautoselector.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.wetek.playerautoselector.extention.OnItemClickListener
import com.wetek.playerautoselector.R
import com.wetek.playerautoselector.extention.addOnItemClickListener
import com.wetek.playerautoselector.databinding.ActivityMainBinding
import com.wetek.playerautoselector.viewmodel.ChannelItemViewModel
import com.wetek.playerautoselector.viewmodel.ChannelListViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var channelList: ChannelListViewModel
    private lateinit var activeChannel : ChannelItemViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.channelList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        channelList = ViewModelProviders.of(this).get(ChannelListViewModel::class.java)
        activeChannel = ViewModelProviders.of(this).get(ChannelItemViewModel::class.java)
        binding.channelList.addOnItemClickListener(object: OnItemClickListener {
            override fun onItemClicked(position: Int, view: View) {
                val channel = channelList.channelListAdapter.channelList[position]
                activeChannel.bind(channel)
                binding.activeChannel = activeChannel
            }
        })
        binding.viewModel = channelList

    }

}
